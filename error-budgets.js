/* eslint-disable no-console */
"use strict";

const { PROMETHEUS_ENDPOINT } = process.env;
const { PrometheusDriver } = require("prometheus-query");
const startOfDay = require("date-fns/start_of_day");

const prom = new PrometheusDriver({
  endpoint: PROMETHEUS_ENDPOINT || "http://localhost:10902",
  timeout: 15000 // 15 seconds
});

const queryBudgets = `
  last_over_time(gitlab:stage_group:availability:ratio_28d{environment="gprd",monitor="global"}[2h])
`;

const queryWeekBudgets = `
  last_over_time(gitlab:stage_group:availability:ratio_7d{environment="gprd",monitor="global"}[2h])
`;

const apdexComponentQuery = `
  last_over_time(gitlab:stage_group:sli_kind:availability:ratio_28d{sli_kind='apdex', environment="gprd", monitor="global"}[2h])
`;

const errorComponentQuery = `
  last_over_time(gitlab:stage_group:sli_kind:availability:ratio_28d{sli_kind='error', environment="gprd", monitor="global"}[2h])
`;

const queryShares = `
  last_over_time(gitlab:stage_group:traffic_share:ratio_28d{environment="gprd", monitor="global"}[2h])
`;

function indexByStageGroup (result) {
  return result.reduce((memo, series) => {
    const stageGroup = series.metric.labels.stage_group;
    memo[stageGroup] = series.value.value;

    return memo;
  }, {});
}

async function instantQuery (query, time) {
  let retries = 3;
  for (;;) {
    try {
      console.error("# promql query");
      return await prom.instantQuery(query, time);
    } catch (e) {
      console.error(`# promql query failed: query=${query}: ${JSON.stringify(e)}`);
      retries--;

      if (retries <= 0) {
        throw new Error(`promql query failed: query=${query}: ${JSON.stringify(e)}`);
      } else {
        // Sleep for a second
        await new Promise((resolve) => setTimeout(resolve, 1000));
      }
    }
  }
}

async function fetchErrorBudgets () {
  const midnight = startOfDay(Date.now());

  const shareResponse = await instantQuery(queryShares, midnight);
  const shares = indexByStageGroup(shareResponse.result);

  const apdexResponse = await instantQuery(apdexComponentQuery, midnight);
  const apdexComponent = indexByStageGroup(apdexResponse.result);

  const errorResponse = await instantQuery(errorComponentQuery, midnight);
  const errorComponent = indexByStageGroup(errorResponse.result);

  const weekBudgetResponse = await instantQuery(queryWeekBudgets, midnight);
  const weekBudget = indexByStageGroup(weekBudgetResponse.result);

  const res = await instantQuery(queryBudgets, midnight);
  const seriesBudgets = res.result;
  return seriesBudgets.reduce((memo, series) => {
    const stageGroup = series.metric.labels.stage_group;

    memo[stageGroup] = {
      budget: series.value.value,
      weekBudget: weekBudget[stageGroup],
      share: shares[stageGroup],
      apdexComponent: apdexComponent[stageGroup],
      errorComponent: errorComponent[stageGroup]
    };

    return memo;
  }, {});
}

module.exports = fetchErrorBudgets;
